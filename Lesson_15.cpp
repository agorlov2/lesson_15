﻿// Lesson_15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>


using namespace std;

int main()
{
	setlocale(LC_ALL, "ru");

	int N, OderNumber;

	cout << "Числа от 0 до N. Введите N : " << endl;
	cin >> N;

	cout << "0 - Чётные, 1 - Нечётные: " << endl;
	cin >> OderNumber;

	for (size_t i = 0; i <= N; i++)
	{
		if (OderNumber == 0)
		{
			if (i % 2 == 0)
			{
				cout << i << "  ";
			}
		}
		else
		{
			if (i % 2 == 1)
			{
				cout << i << "  ";
			}
		}
	}

	return 0;

}